$(document).ready(()=>{
    $("#navbar-panel").width($("#sample-div").outerWidth());
    $(".myInput").width($("#sample-div").outerWidth());


    $("#btnMenuNav").click(()=>{
        if($("#btnMenuNav").hasClass("actived-btn-nav")){
            $("#btnMenuNav").removeClass("actived-btn-nav");
            $("#main-panel").animate({left:0},500,'linear');
            $("#btnMenuNav").animate({left:0},500,'linear');
        }else{
            $("#btnMenuNav").addClass("actived-btn-nav");
            $("#main-panel").animate({left:$("#sample-div").outerWidth()},500,'linear');
            $("#btnMenuNav").animate({left:$("#sample-div").outerWidth()},500,'linear');
        }
    });

    setInterval(()=>{
        $(".list-nav>a").unbind();
        $(".list-nav>a").click((e)=>{
            if($("#btnMenuNav").hasClass("actived-btn-nav")){
                setTimeout(()=>{
                    $("#btnMenuNav").click();
                },100);
            }
            $(".list-nav").children().removeClass("list-active");
            $(e.target).addClass("list-active");
        });
    },100);

    $(window).resize(()=>{
        $("#navbar-panel").width($("#sample-div").outerWidth());
        $(".myInput").width($("#sample-div").outerWidth());
        if(!$("#btnMenuNav").hasClass("actived-btn-nav")){
            $("#main-panel").animate({left:0},0,'linear');
            $("#btnMenuNav").animate({left:0},0,'linear');
        }else{
            $("#main-panel").animate({left:$("#sample-div").outerWidth()},0,'linear');
            $("#btnMenuNav").animate({left:$("#sample-div").outerWidth()},0,'linear');
        }
    });
});