import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./assets/style.css";
import '../node_modules/react-notifications/lib/notifications.css';
import {NotificationContainer} from 'react-notifications';
import Home from "./pages/home";
import Login from "./pages/login";
import Signup from "./pages/signup";
import Publish from "./pages/publish";


export default class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLogin:false,
        };
    }

    componentDidMount(){
        if(localStorage.getItem("token")!==null){
            this.setState({isLogin:true});
        }
    }

    render(){
        let menu = [
            {
                label:"Home",
                icon:"home",
                path:"/",
            },
            {
                label:"Account",
                icon:"account_box",
                path:"/login",
                only_anonymous:true,
            },
            {
                label:"Publish",
                icon:"create",
                path:"/publish",
                only_authenticated:true,
            },
            {
                label:"Logout",
                icon:"logout",
                path:"/logout",
                only_authenticated:true,
            },
        ];
        let nav_list = menu.map(item=>{
            if(this.state.isLogin && item.only_anonymous){
                return;
            }
            if(!this.state.isLogin && item.only_authenticated){
                return;
            }
            if(item.path==="/logout"){
                return(
                    <a href="#" style={{color:"orange"}} onClick={(e)=>{
                        e.preventDefault();
                        localStorage.removeItem("token");
                        this.setState({isLogin:false});
                    }}>
                        {item.label}
                        <i class="material-icons">{item.icon}</i>
                    </a>
                );
            }else{
                return(
                    <Link to={item.path}>
                        {item.label}
                        <i class="material-icons">{item.icon}</i>
                    </Link>
                );
            }
        });
        return(
            <Router>
                <div id="navbar-panel">
                    <div class="row">
                        <div class="col s12 panel-logo">
                            <div class="img-logo center" style={{background:"url(https://images.unsplash.com/photo-1500622944204-b135684e99fd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1601&q=80)"}}></div>
                        </div>
                    </div>

                    <div class="list-nav">
                        {nav_list}
                    </div>
                </div>

                <div id="main-panel">
                    <div id="btnMenuNav">
                        <div class="bar1"></div>
                        <div class="bar2"></div>
                        <div class="bar3"></div>
                    </div>
                    
                    <div class="container">
                        <div class="row">
                            <div class="col s10 m5 l4" id="sample-div"></div>
                        </div>
                        <Switch>
                            <Route exact path="/" component={()=><Home/>}></Route>
                            <Route exact path="/login/" component={()=><Login parent={this}/>}></Route>
                            <Route exact path="/signup/" component={Signup}></Route>
                            <Route exact path="/publish/" component={Publish}></Route>
                        </Switch>
                    </div>
                    <NotificationContainer/>
                </div>
            </Router>
        );
    }
}