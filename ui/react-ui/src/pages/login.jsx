import React from "react";
import { Link,Redirect } from "react-router-dom";
import {NotificationManager} from 'react-notifications';


export default class Login extends React.Component{
    constructor(props){
        super(props);
        this.loginClicked = this.loginClicked.bind(this);

        this.state = {
            username:"",
            password:"",
            toHome:false,
        };
    }

    loginClicked(e){
        var hdata = {
            username:this.state.username,
            password:this.state.password,
        };
        fetch("http://localhost:8000/api/login/",{
            method:"POST",
            body:JSON.stringify(hdata),
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(resp=>{
            if(resp.status!==200){
                throw resp.json();
            }
            return resp.json();
        })
        .then(data=>{
            this.setState({username:"",password:""});
            NotificationManager.success("Login was successful");
            localStorage.setItem("token",data);
            this.setState({toHome:true});
            this.props.parent.setState({isLogin:true});
        })
        .catch(err=>{
            err.then(txt=>{
                NotificationManager.error(txt);
            });
        });
    }

    render(){
        return this.state.toHome===true?(<Redirect to="/"/>):(
            <div>
                <div class="center-screen">
                    <div class="panel-center-panel">
                        <input onChange={(e)=>this.setState({username:e.target.value})} value={this.state.username} placeholder="Username" id="txtUsername" class="myInput" type="text"/>
                        <input onChange={(e)=>this.setState({password:e.target.value})} value={this.state.password} placeholder="Password" id="txtPassword" class="myInput" type="password"/>
                        <div class="center">
                            <a class="a-white" href="#">I forget my password</a>
                        </div>
                        <button onClick={this.loginClicked} class="btn waves-effect waves-light green btnPedram">
                            Login
                        </button>
                        <Link to="/signup" class="btn waves-effect waves-light indigo btnPedram">
                            Register
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}