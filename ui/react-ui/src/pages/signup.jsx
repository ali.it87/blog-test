import React from "react";


export default class Signup extends React.Component{
    render(){
        return (
            <div class="center-screen">
                <div class="panel-center-panel">
                    <br/>
                    <br/>
                    <br/>
                    <div class="col s2 m3 l4"></div>
                    <div class="col s10 m5 l4" style={{"display": "flex","flex-direction": "column","align-items": "center"}}>
                        <input placeholder="Firstname" class="myInput" type="text"/>
                        <input placeholder="Lastname" class="myInput" type="text"/>
                        <input placeholder="Email" class="myInput" type="text"/>
                        <input placeholder="Username" class="myInput" type="text"/>
                        <input placeholder="Password" class="myInput" type="password"/>
                        <input placeholder="Confirm Password" class="myInput" type="password"/>
                        <button class="btn waves-effect waves-light green btnPedram">
                            Register
                        </button>
                    </div>
                    <div class="col s2 m3 l4"></div>
                </div>
            </div>
        );
    }
}