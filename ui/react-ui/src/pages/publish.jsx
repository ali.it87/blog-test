import React from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Authentication from "../components/authtentication";


export default class Publish extends React.Component{
    constructor(props){
        super(props);
        this.state={
            content:"",
        };
    }
    render(){
        return (
            <div>
                <Authentication/>
                <h1>Publish</h1>
                <input class="myInput forceWidth" placeholder="Title" type="text"/>
                <CKEditor
                    editor={ ClassicEditor }
                    data={this.state.content}
                    onInit={ editor => {
                        // You can store the "editor" and use when it is needed.
                    } }
                    onChange={ ( event, editor ) => {
                        const data = editor.getData();
                        this.setState({content:data});
                    } }
                />
                <br/>
                <input type="file"/>
                <br/>
                <br/>
                <button class="btnPedram btn large-btn green">Publish</button>
            </div>
        );
    }
}