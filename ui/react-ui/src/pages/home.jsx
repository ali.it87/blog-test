import React from "react";
import PostPreviewList from "../components/postPreviewList";


export default class Home extends React.Component{
    render(){
        let list = [
            {
                title:"First test",
                date:"2019-03-01",
                author:"Alvin",
                content:"Hello World!",
                likes:"32",
                comments:"3",
                image:"https://images.unsplash.com/photo-1500622944204-b135684e99fd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1601&q=80",
            },
            {
                title:"Second test",
                date:"2019-03-05",
                author:"John",
                content:"This is a test content",
                likes:"21",
                comments:"1",
                image:"https://images.unsplash.com/photo-1494256997604-768d1f608cac?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
            },
        ];
        return(
            <PostPreviewList list={list}/>
        );
    }
}