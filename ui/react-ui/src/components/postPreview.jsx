import React from "react";


export default class PostPreview extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    render(){
        return(
            <div>
                <div class="post-preview row">
                    <div class="col s12 m1 l2"></div>
                    <div class="col s12 m10 l8">
                        <div class="card">
                            <div class="card-image">
                                <img src={this.props.image}/>
                                <div class="overlay-card-image"></div>
                                <div class="card-title">
                                    <h3>{this.props.title}</h3>
                                    <div>{this.props.date}</div>
                                    <div>{this.props.author}</div>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s12">
                                        <p>{this.props.content}</p>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-action">
                                <div class="row">
                                    <div class="col s6">
                                        <div class="row">
                                            <div class="col s12">
                                                <div class="detail-post-container">
                                                    <div class="center-ver">
                                                        <i class="material-icons">favorite</i>
                                                        &nbsp;&nbsp;
                                                        {this.props.likes}
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <div class="center-ver">
                                                        <i class="material-icons">comment</i>
                                                        &nbsp;&nbsp;
                                                        {this.props.comments}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s6 right-align">
                                        <a class="btnText" href="#">
                                            Continue
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m1 l2"></div>
                </div>
            </div>
        );
    }
}