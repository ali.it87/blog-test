import React from "react";
import {Redirect} from "react-router-dom";


export default class Authentication extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            auth:localStorage.getItem('token')===null,
        };
    }
    render(){
        return this.state.auth?(<Redirect to='/'/>):(<div></div>)
    }
}