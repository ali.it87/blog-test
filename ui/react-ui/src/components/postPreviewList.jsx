import React from "react";
import PostPreview from "./postPreview";


export default class PostPreviewList extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    render(){        
        var components = this.props.list.map(item=>{
            return (
                <PostPreview title={item.title} date={item.date}
                author={item.author} content={item.content}
                likes={item.likes} comments={item.comments} image={item.image}/>    
            );
        });
        return(
            components
        );
    }
}