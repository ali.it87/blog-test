import jwt
from datetime import datetime,timedelta
from django.conf import settings
from rest_framework.response import Response
from packs.constants import HTTP_CODES
from django.contrib.auth.models import User


def generate(username):
    now = datetime.now()
    exp = now + timedelta(days=30)
    now = now.strftime("%Y-%m-%d %H:%M:%S.%f")
    exp = exp.strftime("%Y-%m-%d %H:%M:%S.%f")
    payload = {
        "username":username,
        "create":now,
        "expire":exp,
    }
    token = jwt.encode(payload,settings.SECRET_KEY)
    return token

def authenticate(fun):
    def inner(self,request):
        data = request.data
        if data == None:
            return Response("Bad data format", HTTP_CODES.HTTP_BAD_REQ)
        try:
            token_data = jwt.decode(data["token"],key=settings.SECRET_KEY)
            exp = datetime.strptime(token_data["expire"],"%Y-%m-%d %H:%M:%S.%f")
            print(exp)
            if exp < datetime.now():
                return Response("Token is expired", HTTP_CODES.HTTP_UNAUTHORIZED)    
        except:
            return Response("Unauthorized", HTTP_CODES.HTTP_UNAUTHORIZED)
        user = User.objects.get(username=token_data["username"])
        if user == None:
            return Response("User not found!", HTTP_CODES.HTTP_NOT_FOUND)
        self.user = user
        return fun(self,request)
    return inner