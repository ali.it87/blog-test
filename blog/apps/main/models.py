from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    image = models.ImageField(max_length=5000,blank=True,upload_to="images")
    phone = models.CharField(max_length=30,blank=True)

class Comment(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    content = models.TextField(max_length=5000)

class Post(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    title = models.CharField(max_length=300)
    content = models.TextField(max_length=5000)
    publish_date = models.DateTimeField(auto_now=True)
    comments = models.ManyToManyField(Comment)
    image = models.ImageField(max_length=5000,blank=True,upload_to="images")