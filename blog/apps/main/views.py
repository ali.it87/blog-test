from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import LoginSerializer,SignupSerializer
from packs.constants import HTTP_CODES
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from packs.auth import pjwt
from .models import Profile


class Login(APIView):

    def post(self,request):
        serializer = LoginSerializer(data=request.data)
        if not serializer.is_valid():
            return Response("Bad data format", HTTP_CODES.HTTP_BAD_REQ)
        data = serializer.validated_data
        uname = data["username"]
        passw = data["password"]
        result = authenticate(username=uname,password=passw)
        if not result:
            return Response("Username or password is wrong", HTTP_CODES.HTTP_UNAUTHORIZED)
        user = User.objects.get(username=uname)
        token = pjwt.generate(user.username)
        return Response(token, HTTP_CODES.HTTP_OK)


class Signup(APIView):
    
    def post(self,request):
        serilizer = SignupSerializer(data = request.data)
        if not serilizer.is_valid():
            if "already" in str(serilizer.errors["username"]):
                return Response("Username already exists", HTTP_CODES.HTTP_BAD_REQ)    
            return Response("Bad data format", HTTP_CODES.HTTP_BAD_REQ)
        user = serilizer.save()
        profile = Profile()
        profile.user = user
        profile.save()
        return Response("Registeration was successful", HTTP_CODES.HTTP_OK)


class Publish(APIView):

    @pjwt.authenticate
    def post(self,request):
        return Response(f"published! dear {self.user.username}", HTTP_CODES.HTTP_OK)