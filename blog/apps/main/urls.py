from django.urls import path
from .views import Login,Publish,Signup


urlpatterns = [
    path('login/', Login.as_view()),
    path('publish/', Publish.as_view()),
    path('signup/', Signup.as_view()),
]
